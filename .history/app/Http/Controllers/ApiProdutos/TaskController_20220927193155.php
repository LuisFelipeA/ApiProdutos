<?php

namespace App\Http\Controllers\ApiProdutos;

use App\Http\Controllers\Controller;
use App\Models\produtos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::all();

        return response()->json($tasks);
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = Validator::make($request->all(), [
            'name' => 'required|max:255|min:10',
            'status' => 'required',
            'date' => 'required'
        ]);

        if ($validated->fails()) {
            return response()->json([
                'message' => 'Your request is missing data'
            ], 400);
        }

        $task = Task::create($request->all());

        return response()->json([
            'message' => 'Task created successfully',
            'task' => $task
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\produtos  $produtos
     * @return \Illuminate\Http\Response
     */
    public function show(produtos $produtos)
    {
        return response()->json([
            'message' => 'success',
            'task' => $task
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\produtos  $produtos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, produtos $produtos)
    {
        return response()->json([
            'message' => 'Task updated successfully',
            'task' => $task
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\produtos  $produtos
     * @return \Illuminate\Http\Response
     */
    public function destroy(produtos $produtos)
    {
        $task->delete();

        return response()->json([
            'message' => "Task deleted successfully"
        ], 200);
    }
}













    public function destroy(Task $task)
    {

    }
}
